package com.example.demo.vgs;

import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.example.demo.vgs.chain.SingleThreadChainExecutor;
import com.example.demo.vgs.chain.ThreadChainFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class VGSMainTest {

    @Mock
    ThreadChainFactory threadChainFactory;

    @InjectMocks
    ThreadHandler threadHandler = new ThreadHandler();

    @Test
    public void test_chainSetup() {
        // given
        SingleThreadChainExecutor singleThreadChainExecutor1 = mock(SingleThreadChainExecutor.class);
        SingleThreadChainExecutor singleThreadChainExecutor2 = mock(SingleThreadChainExecutor.class);
        SingleThreadChainExecutor singleThreadChainExecutor3 = mock(SingleThreadChainExecutor.class);

        when(threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE,
                threadHandler.getServiceA())).thenReturn(singleThreadChainExecutor1);

        when(threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE,
                threadHandler.getServiceB())).thenReturn(singleThreadChainExecutor2);

        when(threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE,
                threadHandler.getServiceC())).thenReturn(singleThreadChainExecutor3);

        doNothing().when(singleThreadChainExecutor1).execute();

        // when
        threadHandler.execute();

        // then verify order of thread chains execution
        verify(singleThreadChainExecutor1).setNext(singleThreadChainExecutor2);
        verify(singleThreadChainExecutor2).setNext(singleThreadChainExecutor3);
        verify(singleThreadChainExecutor1).execute();
    }

}