package com.example.demo.vgs.chain;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import com.example.demo.vgs.service.ServiceA;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.MockitoJUnitRunner;

@RunWith(MockitoJUnitRunner.class)
public class ThreadChainExecutorTest {

    @Spy
    ThreadChainExecutor threadChainExecutor = new SingleThreadChainExecutor(new ServiceA());

    @Test
    public void test_threadChainExecutor_verifyOrder() throws InterruptedException {
        // given
        Thread thread = mock(Thread.class);
        when(threadChainExecutor.createThreads()).thenReturn(thread);

        // when
        threadChainExecutor.execute();

        // then verify join operation after start
        InOrder inOrder = Mockito.inOrder(thread);
        inOrder.verify(thread).start();
        inOrder.verify(thread).join();
    }

}