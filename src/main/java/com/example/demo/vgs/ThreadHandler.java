package com.example.demo.vgs;

import com.example.demo.vgs.chain.ThreadChainExecutor;
import com.example.demo.vgs.chain.ThreadChainFactory;
import com.example.demo.vgs.service.Service;
import com.example.demo.vgs.service.ServiceA;
import com.example.demo.vgs.service.ServiceB;
import com.example.demo.vgs.service.ServiceC;

/**
 * Used for creating chain of Thread executors
 * Chain can be viewed as ordered graph
 * Each element of graph can work with single or multiply threads and perform some Service logic
 */
public class ThreadHandler {

    private ThreadChainFactory threadChainFactory = new ThreadChainFactory();
    // TODO can be injected into list/map
    private Service serviceA = new ServiceA();
    private Service serviceB = new ServiceB();
    private Service serviceC = new ServiceC();

    public void execute() {
        ThreadChainExecutor threadChainExecutor1 =
                threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE, serviceA);
        ThreadChainExecutor threadChainExecutor2 =
                threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE, serviceB);
        ThreadChainExecutor threadChainExecutor3 =
                threadChainFactory.createThreadChainExecutor(ThreadChainFactory.ThreadChainExecutorType.SINGLE, serviceC);

        // Setup order
        threadChainExecutor1.setNext(threadChainExecutor2);
        threadChainExecutor2.setNext(threadChainExecutor3);

        // start execution
        threadChainExecutor1.execute();
    }

    public Service getServiceA() {
        return serviceA;
    }

    public Service getServiceB() {
        return serviceB;
    }

    public Service getServiceC() {
        return serviceC;
    }

    public void setServiceA(Service serviceA) {
        this.serviceA = serviceA;
    }

    public void setServiceB(Service serviceB) {
        this.serviceB = serviceB;
    }

    public void setServiceC(Service serviceC) {
        this.serviceC = serviceC;
    }
}
