package com.example.demo.vgs.chain;

import com.example.demo.vgs.service.Service;

/**
 * used for creating synch/asynch steps in chain
 * with different thread executors (single, multiply)
 */
public abstract class ThreadChainExecutor {

    private ThreadChainExecutor next;
    protected Service service;

    private Thread.UncaughtExceptionHandler
            exceptionHandler = (th, ex) ->
            //TODO write logic for thread exception handler if needed
            System.out.println("Uncaught exception: " + ex);

    public ThreadChainExecutor(Service service) {
        if (service == null) {
            throw new IllegalArgumentException("service should be provided");
        }
        this.service = service;
    }

    protected abstract Thread createThreads();

    public final void execute() {
        Thread thread = createThreads();

        thread.setUncaughtExceptionHandler(exceptionHandler);
        thread.start();
        try {
            thread.join();
        } catch (InterruptedException e) {
            System.out.println("Thread was interrupted " + e.getMessage());
            // TODO write additional logic if needed
        }

        if (next != null) {
            next.execute();
        }
    }

    public void setNext(ThreadChainExecutor next) {
        this.next = next;
    }

    public ThreadChainExecutor getNext() {
        return next;
    }
}
