package com.example.demo.vgs.chain;

import com.example.demo.vgs.service.Service;

public class ThreadChainFactory {

    public ThreadChainExecutor createThreadChainExecutor(ThreadChainExecutorType threadChainType, Service service) {
        if (threadChainType == ThreadChainExecutorType.SINGLE) {
            return new SingleThreadChainExecutor(service);
        } else {
            throw new UnsupportedOperationException();
        }
    }

    public enum ThreadChainExecutorType {
        SINGLE, MULTIPLY
    }

}
