package com.example.demo.vgs.chain;

import com.example.demo.vgs.service.Service;

/**
 * Used for creating single thread per chain, asynch operation
 */
public class SingleThreadChainExecutor extends ThreadChainExecutor {

    public SingleThreadChainExecutor(Service service) {
        super(service);
    }

    @Override
    protected Thread createThreads() {
        // TODO result can be passed from one chain to another if needed
        // Future interface should be used in that case
        return new Thread(service::doLogic);
    }
}
