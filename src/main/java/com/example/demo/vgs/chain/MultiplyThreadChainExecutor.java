package com.example.demo.vgs.chain;

import com.example.demo.vgs.service.Service;

/**
 * Used for creating pool of threads per chain ForkJoinPoll etc, asynch operation
 */
public class MultiplyThreadChainExecutor extends ThreadChainExecutor {

    public MultiplyThreadChainExecutor(Service service) {
        super(service);
    }

    @Override
    protected Thread createThreads() {
        throw new UnsupportedOperationException();
    }
}
