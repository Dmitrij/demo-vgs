package com.example.demo.vgs.service;

/**
 * Logic for thread execution
 */
public abstract class Service {

    protected Foo foo = new Foo();

    public abstract void doLogic();
}
